'use strict';

/**
 * @ngdoc overview
 * @name techingenApp
 * @description
 * # techingenApp
 *
 * Main module of the application.
 */
angular
  .module('techingenApp', [
    'ui.router',
    'ngMessages'
  ]);
