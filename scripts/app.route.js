angular.module('techingenApp')
    .config(function($stateProvider, $urlRouterProvider,$locationProvider) {

        $urlRouterProvider.otherwise('/home');
        //---------------------------------------------------------------------------------------
        // Route Providers
        //---------------------------------------------------------------------------------------
        $stateProvider

        // Home
        .state({
            name: 'home',
            url: '/home',
            templateUrl: 'modules/home/home.template.html',
            controller: 'homeCtrl'
        })

        // Services
        .state({
            name: 'charts',
            url: '/charts',
            templateUrl: 'modules/charts/charts.template.html',
            controller: ''
        })
		
        .state({
            name: 'tables',
            url: '/tables',
            templateUrl: 'modules/tables/tables.template.html',
            controller: ''
        })	
		
        .state({
            name: 'forms',
            url: '/forms',
            templateUrl: 'modules/forms/forms.template.html',
            controller: ''
        })	

        .state({
            name: 'bootstrap-elements',
            url: '/bootstrap-elements',
            templateUrl: 'modules/bootstrap-elements/bootstrap-elements.template.html',
            controller: ''
        })		

        //add more routes here
        
        // use the HTML5 History API , remove hash from url
        $locationProvider.html5Mode(true);
    });
