'use strict';

/**
 * @ngdoc function
 * @name techingenApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the techingenApp
 */
angular.module('techingenApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
