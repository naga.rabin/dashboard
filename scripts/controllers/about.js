'use strict';

/**
 * @ngdoc function
 * @name techingenApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the techingenApp
 */
angular.module('techingenApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
